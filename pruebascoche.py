import unittest
from codigocoche import Coche

class TestCoche(unittest.TestCase):
    def test_constructor(self):
        mi_coche = Coche('Rojo', 'Ford', 'Focus', '6387GYH', 0)
        self.assertEqual(self.mi_coche.color, "Rojo")
        self.assertEqual(self.mi_coche.marca, "Ford")
        self.assertEqual(self.mi_coche.modelo, "Focus")
        self.assertEqual(self.mi_coche.matricula, "6387GYH")
        self.assertEqual(self.mi_coche.velocidad, 0)

    def test_acelera(self):
        mi_coche = Coche('Azul', 'Fiat', '500', '8201KLO', 0)
        mi_coche.acelera(20)
        self.assertEqual(self.mi_coche.velocidad, 20)

    def test_frena(self):
        mi_coche = Coche('Blanco', 'Volkswagen', 'Scirocco', '2457ADK', 30)
        mi_coche.frena(10)
        self.assertEqual(mi_coche.velocidad, 20)

if __name__ == '__main__':
    unittest.main()