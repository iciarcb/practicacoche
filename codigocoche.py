class Coche:
    def __init__(self, color, marca, modelo, matricula, velocidad=0):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad

    def acelera(self, cantidad):
        self.velocidad += cantidad

    def frena(self, cantidad):
        self.velocidad -= cantidad
        if self.velocidad < 0:
            self.velocidad = 0

mi_coche = Coche('Rojo', 'Ford', 'Focus', '6387GYH', 120)
print('Descripción del coche: color', mi_coche.color, ';marca:', mi_coche.marca, ';modelo:', mi_coche.modelo, ';matrícula:', mi_coche.matricula, ';velocidad:', mi_coche.velocidad)

mi_coche.acelera(10)
print('La velocidad del coche después de acelerar es de:', mi_coche.velocidad)

mi_coche.frena(40)
print('La velocidad del coche después de frenar es de:', mi_coche.velocidad)